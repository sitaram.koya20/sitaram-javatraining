package com.virtusa.training.TrainingRest.service;

import com.virtusa.training.TrainingRest.model.Employee;
import com.virtusa.training.TrainingRest.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Iterable<Employee> listAllEmployees(){
        return employeeRepository.findAll();
    }

    public void saveEmployee(Employee employee){
        employeeRepository.save(employee);
    }

    public Employee getEmployee(Integer id){
        return employeeRepository.findById(id).get();
    }

    public void deleteEmployee(Integer id){
        employeeRepository.deleteById(id);
    }
}