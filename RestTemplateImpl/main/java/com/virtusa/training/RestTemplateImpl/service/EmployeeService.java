package com.virtusa.training.RestTemplateImpl.service;

import com.virtusa.training.RestTemplateImpl.model.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getEmployees();
}
