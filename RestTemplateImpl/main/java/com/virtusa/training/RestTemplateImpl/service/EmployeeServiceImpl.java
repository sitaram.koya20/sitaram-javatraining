package com.virtusa.training.RestTemplateImpl.service;

import com.virtusa.training.RestTemplateImpl.model.Employee;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final RestTemplate restTemplate = new RestTemplate();
    @Override
    public List<Employee> getEmployees() {
        return restTemplate.exchange("http://localhost:8080/employees", HttpMethod.GET, null, new ParameterizedTypeReference<List<Employee>>() {}).getBody();
    }
}
