package com.virtusa.training.RestTemplateImpl.controller;

import com.virtusa.training.RestTemplateImpl.model.Employee;
import com.virtusa.training.RestTemplateImpl.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DataController {
    @Autowired
    EmployeeService employeeService;

    @RequestMapping("/getEmployees")
    public List<Employee> getEmployees(){
        return employeeService.getEmployees();
    }
}
