package com.virtusa.training.RestTemplateImpl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestTemplateImplApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestTemplateImplApplication.class, args);
	}

}
